import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { UsuarioService } from 'src/services/usuario.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-lista-usuarios',
  templateUrl: './lista-usuarios.component.html',
  styleUrls: ['./lista-usuarios.component.css']
})
export class ListaUsuariosComponent implements OnInit {

  lista: any[] = [];

  constructor(private http: HttpClient, private servicio: UsuarioService, private router: Router) {

    servicio.getAllUsuarios().subscribe(resp => 
      this.lista = resp);

   }

  ngOnInit() {
  }

  public verCompletados(userId: number){
    this.router.navigate(['listaToDos/' +userId +'/true']);
  }

  public verIncompletos(userId: number){
    this.router.navigate(['listaToDos/' +userId +'/false']);
  }

}
