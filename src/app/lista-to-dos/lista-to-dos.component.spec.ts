import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaToDosComponent } from './lista-to-dos.component';

describe('ListaToDosComponent', () => {
  let component: ListaToDosComponent;
  let fixture: ComponentFixture<ListaToDosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaToDosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaToDosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
