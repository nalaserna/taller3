import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(private http: HttpClient) {

   }

   public getAllUsuarios(): Observable<any[]> {
    return this.http.get<any[]>(environment.listaUsuarios);
  }

  public getAllToDos(): Observable<any[]>{
    return this.http.get<any[]>(environment.listaToDos);
  }

  public getToDosByStatus(userId: string, completed: boolean){
    const body= new HttpParams().set('userId', userId).set('completed', completed+'');
    return this.http.get<any[]>(environment.listaToDos, {params: body});
  }

  
} 
